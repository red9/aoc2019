cmake_minimum_required(VERSION 3.12)
project(aoc2019 C)

set(CMAKE_C_STANDARD 99)

add_executable(day1 d1-10/day1.c)
add_executable(day2 intcode/day2.c)
add_executable(day3 d1-10/day3.c)
add_executable(day4 d1-10/day4.c)
add_executable(day5 intcode/day5.c intcode/intcode.c)
add_executable(day6 d1-10/day6.c)
add_executable(day7 intcode/day7.c intcode/intcode.c)
add_executable(day8 d1-10/day8.c)
add_executable(day9 intcode/day9.c intcode/intcode.c)