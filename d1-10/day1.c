#include <stdio.h>
#include <stdlib.h>

int main() {
    unsigned sum = 0;
    while (1) {
        int a = 0;
        scanf("%d\n", &a);
        if(!a) { break; }
        while (a > 6) {
            a = (a / 3) - 2;
            sum += a;
        }
    }
    printf("Sum: %u\n", sum);
    return EXIT_SUCCESS;
}