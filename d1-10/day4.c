//
// Created by red on 04/12/19.
//

#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>

int power(int base, int pow){
  if (pow <= 0) return 1;
  return base*power(base,pow-1);
}

int main(){
  const int lower = 152085;
  const int upper = 670283;
  int digits[6];
  int ctr = 0;
  for (size_t i = lower; i <= upper; i++){
    for (size_t j = 0; j < 6; j++) {
      int mult = power(10, j);
      digits[j] = ((int) i % (10*mult)) / mult;
    }
    bool dd = false;
    bool increasing = true;
    int run = 1;
    for (size_t j = 1; j < 6; j++) {
      if (digits[j] == digits[j-1]) run++;
      else {
        if (run == 2) dd = true;
        run = 1;
      }
      if (digits[j] > digits[j-1]) increasing = false;
    }
    if (run == 2) dd = true;
    if (dd && increasing) ctr++;
  }
  printf("%d numbers found\n", ctr);
  return EXIT_SUCCESS;
}

