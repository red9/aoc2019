//
// Created by red on 08/12/19.
//

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main(int argv, char** argc) {
  char* filename = argc[1];
  FILE* f = fopen(filename, "r");
  char buffer[15001];
  fscanf(f, "%s", buffer);
  char final[150];
  memset((void*) final, '2', 150*sizeof(char));
  int min_0s = 150;
  int checksum = 0;
  for (size_t layer = 0; layer < 100; layer++) {
    int num_0s = 0;
    int num_1s = 0;
    int num_2s = 0;
    for (size_t i = 0; i < 150; i++) {
      char c = buffer[layer*150 + i];
      switch (c) {
      case '0': num_0s++; break;
      case '1': num_1s++; break;
      case '2': num_2s++; break;
      default: break;
      }
      if (final[i] == '2' && c != '2') final[i] = c;
    }
    if (num_0s < min_0s) {
      min_0s = num_0s;
      checksum = num_1s * num_2s;
    }
  }
  printf("Smallest no. of 0s found: %d, checksum: %d\n", min_0s, checksum);
  for (size_t i = 0; i < 150; i++) {
    printf("%c", final[i]);
    if (i % 25 == 24) {
      puts("");
    }
  }
}