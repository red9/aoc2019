//
// Created by red on 05/12/19.
//

#include <stddef.h>

#ifndef AOC2019_INTCODE_H
#define AOC2019_INTCODE_H


enum {IERR_BAD_OP, IERR_OUT_OF_BOUNDS};
enum {IVAL_OUT, IVAL_ERR, IVAL_END, IVAL_CONT};
typedef struct {
  int type;
  long num;
  int err;
} ival;
typedef struct {
  long* tape;
  size_t tape_l;
  size_t index;
  long* input_tape;
  long relative_base;
} intcode;
void ival_print(ival v);
ival step (intcode* m);
ival run (intcode* m);
intcode* read_file(char* filename, size_t buf_size);
intcode* clone(intcode* m, size_t buf_size);
#endif //AOC2019_INTCODE_H
