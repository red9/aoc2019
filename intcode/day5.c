//
// Created by red on 05/12/19.
//

#include <stdio.h>
#include <stdlib.h>
#include "intcode.h"

int main(int argc, char **argv) {
  char* filename = argv[1];
  intcode* v = read_file(filename, 2048);
  long input[1] = { 5 };
  v->input_tape = input;
  ival out_val = run(v);
  ival_print(out_val);
}