//
// Created by red on 10/12/19.
//

#include "intcode.h"

int main(int argc, char **argv) {
  char *filename = argv[1];
  intcode *v = read_file(filename, 2 << 24);
  long input[1] = { 2 };
  v->input_tape = input;
  for (;;) {
    ival out_val = run(v);
    ival_print(out_val);
    if (out_val.type == IVAL_END || out_val.type == IVAL_ERR) break;
  }
}