//
// Created by red on 07/12/19.
//

#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include "intcode.h"

bool in (int* arr, size_t len, int key) {
  for (size_t i = 0; i < len; i++){
    if (arr[i] == key) return true;
  }
  return false;
}

int main(int argc, char **argv) {
  char* filename = argv[1];
  intcode* v = read_file(filename, 2048);
  long max = 0;
  for(size_t i = 0; i < 3125; i++) {
    int bits[5];
    int num = (int) i;
    for(size_t j = 0; j < 5; j++) {
      bits[j] = num % 5; // read the jth bit
      num = num / 5; // move to the next bit
    }
    bool isperm = true;
    for(size_t j = 0; j < 5; j++) {
      if (in(bits+j+1, 5-j-1, bits[j])) {
        isperm = false;
      }
    }
    if (isperm == false) continue;
    long input[2] = {0, 0};
    for(size_t j = 0; j < 5; j++) {
      intcode* m = clone(v, 2048);
      input[0] = bits[j];
      m->input_tape = input;
      ival out_val = run(m);
      input[1] = out_val.num;
    }
    if (input[1] > max) max = input[1];
  }
  printf("max: %ld\n", max);
  return EXIT_SUCCESS;
}